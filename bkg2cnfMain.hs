--
-- FLP project 1: bkg-2-cnf
-- Created by David Spilka (xspilk00) on 10.4.16.
--

module Main where

-- Library imports
import System.Environment
import System.IO
import System.Exit

-- Our imports
import Bkg2CnfFunctions
import Bkg2CnfData

main :: IO ()
main = do
    args <- getArgs
    let (settings, inFile) = procArgs args

    content <- getContent inFile

    bkg <- getResultedBKG settings content

    putStrLn $ show bkg
    
    return ()

-- Gets context-free grammar from input in the form specified by param
getResultedBKG :: Int -> String -> IO BKG
getResultedBKG param content
    | param==0 = getBKG content
    | param==1 = getBKGWithoutSimpleRules content
    | param==2 = getBKGInCNF content

-- Reads input into a string
getContent :: [Char] -> IO String
getContent [] = getContents
getContent x = readFile x

-- Parse list of arguments into couple
procArgs :: [String] -> (Int,String)
procArgs [] = error "expects 1-2 arguments"
procArgs [x] = procArgs [x, ""]
procArgs [x,y]
    | x=="-i" = (0, y)
    | x=="-1" = (1, y)
    | x=="-2" = (2, y)
    | otherwise = error "unknown argument"
procArgs _ = error "expects 2 arguments"