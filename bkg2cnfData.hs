--
-- FLP project 1: bkg-2-cnf
-- Created by David Spilka (xspilk00) on 10.4.16.
--

module Bkg2CnfData where

-- Library imports
import Data.List

-- Types for nonterminals and terminals
type Nonterminal = String
type Terminal = String

-- Type for rules 
data Rule = Rule
    { left :: Nonterminal
    , right :: [String]
    } deriving (Eq)

instance Show Rule where
    show (Rule l r) = l ++ "->" ++ concat (intersperse "" r)

-- Type for context-free grammar
data BKG = BKG
    { nonterminals :: [Nonterminal]
    , terminals :: [Terminal]
    , start :: Nonterminal
    , rules :: [Rule]
    } deriving (Eq)

instance Show BKG where
    show (BKG n t s r) = concat (intersperse "," n) ++ "\n" ++ concat (intersperse "," t) ++ "\n" ++ s ++ "\n" ++ concat (intersperse "\n" (map show r))